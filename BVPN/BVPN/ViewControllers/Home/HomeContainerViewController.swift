//
//  HomeContainerViewController.swift
//  BVPN
//
//  Created by admin on 8/1/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit

extension NSNotification.Name {
    static let DeleteAccountNotification = Notification.Name("DeleteAccountNotification")
}

class HomeContainerViewController: BaseViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblMemberShip: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblExpiredDate: UILabel!
    
    var currentPageIndex: Int = 0
    var connectVPNVC: HomeViewController!
    var serverListVC: VPNListViewController!
    private var viewControllers: [UIViewController] = []
    private var pageViewController: UIPageViewController!
    var hasTopNotch: Bool {
        if #available(iOS 11.0,  *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupPageViewController()
        showTerm()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(doLogout),
                                               name: Notification.Name.DeleteAccountNotification,
                                               object: nil)
    }
    
    func showTerm() {
        let isAgree = Util.Storage.getBool(key: "AGREE_TERM")
        if !isAgree {
            let webTermVC = WebViewViewController.newVC(url: "term_bvpn", title: "Term & Conditions", isLocalFile: true,isNeedConfirm: true)
            let nav = UINavigationController.init(rootViewController: webTermVC)
            nav.modalPresentationStyle = UIModalPresentationStyle.fullScreen
            self.present(nav, animated: true) {
//                Util.Storage.saveBool(value: true,key: "AGREE_TERM")
            }
        }
    }
    
    func setupUI() {
        lblEmail.text = UserRespository.shared.userInfo?.email ?? ""
        lblMemberShip.text = UserRespository.shared.userInfo?.memberShip?.uppercased() ?? ""
        if let expired = UserRespository.shared.userInfo?.expiredDate, !expired.isEmpty {
            lblExpiredDate.text = "Valid until \(expired)"
        } else {
            lblExpiredDate.text = ""
        }
        if !hasTopNotch {
            self.topViewHeight.constant = 105;
        }
    }
    
    
    func setupPageViewController() {
        self.connectVPNVC  = HomeViewController()
        self.serverListVC = VPNListViewController()
        
        self.viewControllers = [connectVPNVC, serverListVC]
        self.pageViewController = UIPageViewController.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: [:])
        self.pageViewController.setViewControllers([self.connectVPNVC], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
        self.viewContainer.addSubview(self.pageViewController.view)
        self.addChildViewController(self.pageViewController)
        self.pageViewController.dataSource = self
        self.pageViewController.delegate = self
        self.pageViewController.didMove(toParentViewController: self)
        
        viewContainer.translatesAutoresizingMaskIntoConstraints = false
        pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        
        // Pins pageViewController view into the container view on all edges
        viewContainer.topAnchor.constraint(equalTo: pageViewController.view.topAnchor).isActive = true
        viewContainer.bottomAnchor.constraint(equalTo: pageViewController.view.bottomAnchor).isActive = true
        viewContainer.leadingAnchor.constraint(equalTo: pageViewController.view.leadingAnchor).isActive = true
        viewContainer.trailingAnchor.constraint(equalTo: pageViewController.view.trailingAnchor).isActive = true
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UI Actions
    
    @IBAction func actionSetting(_ sender: Any) {
        let nav = UINavigationController.init(rootViewController: SettingViewControlller())
        nav.modalPresentationStyle = .fullScreen
        self.present(nav, animated: true) {
            
        }
    }
    
    @IBAction func actionLogout(_ sender: Any) {
        self.confirm(title: "Logout", message: "Are you sure you want to logout.This action will disconnect VPN", leftBtn: "Cancel", rightBtn: "OK") { (btnIndex) in
            if btnIndex ==  1 {
                //Remove old VPN config
                self.doLogout()
            }
        }
    }
    
   @objc func doLogout() {
        _ = VPNManager.shared.removeVPNConfig()
        self.logout()
    }
    
    func logout() {
        if(!VPNManager.shared.isDisconnected) {
            VPNManager.shared.disconnect()
        }
        self.showHUD()
        API.User.logout(completion: {[weak self] (_, _) in
            guard let `self` = self else {return}
            self.hideHUD()
            UserRespository.shared.logout()
            self.openLoginScreen()
        }) { [weak self] (_) in
            guard let `self` = self else {return}
            self.hideHUD()
            UserRespository.shared.logout()
            self.openLoginScreen()
        }
    }
    
    func openLoginScreen() {
        let loginVC = self.navigationController?.viewControllers[1]
        if loginVC is LoginViewController {
            self.navigationController?.popToViewController(loginVC!, animated: true)
        } else {
            self.navigationController?.viewControllers.insert(LoginViewController(), at: 1)
            self.navigationController?.popToViewController((self.navigationController?.viewControllers[1])!, animated: true)
        }
    }
    
    func updateServerInfo() {
        self.connectVPNVC.updateServerInfo()
    }
    
    func gotoPage(index: Int) {
        if self.currentPageIndex != index {
            let count = 2 //Function to get number of viewControllers
            if index < count {
                if index > currentPageIndex {
                    let vc = self.viewControllers[index]
                    self.pageViewController.setViewControllers([vc], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: { (_) -> Void in
                        self.currentPageIndex = index
                    })
                    
                } else if index < currentPageIndex {
                    let vc = self.viewControllers[index]
                    self.pageViewController.setViewControllers([vc], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: { (_) -> Void in
                        self.currentPageIndex = index
                    })
                }
            }
        }
    }
    
    // MARK: - PageViewControllerDataSource & Delegate
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            self.pageControl.currentPage = self.currentPageIndex
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        self.currentPageIndex = self.viewControllers.index(of: pendingViewControllers.first!) ?? 0
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = viewControllers.index(of: viewController) else { return nil }
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0 else { return viewControllers.last }
        guard viewControllers.count > previousIndex else { return nil }
        return viewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = viewControllers.index(of: viewController) else { return nil }
        let nextIndex = viewControllerIndex + 1
        guard nextIndex < viewControllers.count else { return viewControllers.first }
        guard viewControllers.count > nextIndex else { return nil }
        return viewControllers[nextIndex]
    }
}
