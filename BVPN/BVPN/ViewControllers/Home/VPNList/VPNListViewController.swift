//
//  VPNListViewController.swift
//  BVPN
//
//  Created by admin on 7/31/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit
import PlainPing
import DropDown

enum SortType: Int {
    case Name = 1
    case Latency = 2
    case Favourite = 3
    
    var title:String {
        switch self {
        case .Name:
            return "Name"
        case .Latency:
            return "Latency"
        case .Favourite:
            return "Favourite"
        }
    }
}

class VPNListViewController: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var labelSortBy: UILabel!
    @IBOutlet weak var buttonSortBy: UIButton!
    
    @IBAction func actionSelectSort(_ sender: Any) {
        self.dropDown.show()
    }
    
    var dropDown:DropDown!
    var pingIndex: Int = 0
    var currentSortType:SortType = .Latency
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.pingAndSort()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func setupUI() {
        tblView.tableFooterView = UIView()
        tblView.layoutMargins = UIEdgeInsets.zero
        tblView.separatorInset = UIEdgeInsets.zero
        tblView.registerCell(classCell: ServerCell.self)
        tblView.addRefreshControl(callBack: #selector(refresh), viewController: self)
        
        dropDown = DropDown()
        dropDown.anchorView = self.buttonSortBy
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.dataSource = ["Name","Latency", "Favourite"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.labelSortBy.text = item
            self.currentSortType = SortType.init(rawValue: index + 1) ?? .Latency
            self.sortServerListBy(sortType: self.currentSortType)
            Util.Storage.saveInt(value: self.currentSortType.rawValue, key: "SORT_TYPE")
        }
        dropDown.width = 150
        
        self.currentSortType =  SortType.init(rawValue: Util.Storage.getInt(key: "SORT_TYPE")) ?? .Latency
        self.labelSortBy.text = self.currentSortType.title
        self.sortServerListBy(sortType: self.currentSortType)
    }
    
    
    func sortServerListBy(sortType:SortType) {
        switch sortType {
        case .Name:
            self.sortServerByName()
        case .Latency:
            self.sortServerByLatency()
        case .Favourite:
            self.sortServerByFav()
        }
        self.tblView.reloadData()
    }
    
    func pingAndSort() {
        pingIndex = 0
        self.getServerPing { (_) in
            self.sortServerListBy(sortType: self.currentSortType)
            self.tblView.reloadData()
        }
        
    }
    
    func getServerPing(callback: ((Bool) -> Void)?) {
        if pingIndex == VPNServerManager.shared.serverList.count {
            callback?(true)
            return
        }
        let server = VPNServerManager.shared.serverList[pingIndex]
        PlainPing.ping(server.pingDNS, withTimeout: 2.0, completionBlock: { (timeElapsed: Double?, error: Error?) in
            self.pingIndex += 1
            print("current ping index \(self.pingIndex)")
            if let latency = timeElapsed {
                print(Date().toFormat(output: "hhmmss") + "Ping value :\(latency)")
                server.ping = Int(latency)
            }
            if let error = error {
                print("Ping value error: \(error.localizedDescription) for host :\(server.pingDNS)" )
                server.ping = 10000
            }
            self.getServerPing(callback: callback)
        })
    }
    
    func sortServerByLatency() {
        VPNServerManager.shared.serverList = VPNServerManager.shared.serverList.sorted { (server1, server2) -> Bool in
            return server2.ping >= server1.ping
        }
    }
    
    func sortServerByName() {
        VPNServerManager.shared.serverList = VPNServerManager.shared.serverList.sorted { (server1, server2) -> Bool in
            return server2.name.compare(server1.name).rawValue == 1
        }
    }
    func sortServerByFav() {
        VPNServerManager.shared.serverList = VPNServerManager.shared.serverList.sorted { (server1, server2) -> Bool in
            return (server2.isFavourite.hashValue,server1.ping) < ((server1.isFavourite.hashValue,server2.ping))
        }
    }
    
    @objc func refresh() {
        API.VPN.getListServer(completion: { [weak self] (_, json) in
            guard let `self` = self else {return}
            if let serverList: [ServerInfo] = ServerInfo.listItems(fromJSON: json) as? [ServerInfo] {
                VPNServerManager.shared.serverList = serverList
            }
            self.tblView.endRefresh()
            self.tblView.reloadData()
            self.pingAndSort()
        }) { [weak self] (_) in
            guard let `self` = self else {return}
            self.tblView.endRefresh()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK: TableViewDelegate , TableViewDataSource

extension VPNListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return VPNServerManager.shared.serverList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: ServerCell = tableView.dequeueReusableCell(withIdentifier: String(describing: ServerCell.self)) as? ServerCell {
            cell.config(server: VPNServerManager.shared.serverList[indexPath.row])
            cell.delegate = self
            cell.layoutMargins = UIEdgeInsets.zero
            return cell
        }
        return UITableViewCell()
    }
    
}

// MARK: - ServerCellDelegate {
extension VPNListViewController: ServerCellDelegate {
    
    func selectServer(serverInfo: ServerInfo) {
        VPNServerManager.shared.selectServer = serverInfo
        self.tblView.reloadData()
        if let parentVC = self.parent?.parent as? HomeContainerViewController {
            parentVC.gotoPage(index: 0)
            parentVC.updateServerInfo()
        }
    }
    
    func onUpdateFavor(serverInfo: ServerInfo) {
        if serverInfo.isFavourite && !VPNServerManager.shared.favoriteServers.contains(serverInfo.name) {
            VPNServerManager.shared.favoriteServers.append(serverInfo.name)
        } else if !serverInfo.isFavourite && VPNServerManager.shared.favoriteServers.contains(serverInfo.name) {
            VPNServerManager.shared.favoriteServers.remove(at: VPNServerManager.shared.favoriteServers.index(of: serverInfo.name) ?? -1)
        }
        VPNServerManager.shared.saveFavourite()
        self.sortServerListBy(sortType: self.currentSortType)
    }
}
