//
//  ServerCell.swift
//  BVPN
//
//  Created by admin on 7/31/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit

protocol ServerCellDelegate {
    func selectServer(serverInfo: ServerInfo)
    func onUpdateFavor(serverInfo: ServerInfo)
}

class ServerCell: UITableViewCell {

    @IBOutlet weak var imgCountry: UIImageView!

    @IBOutlet weak var lblLocation: UILabel!

    @IBOutlet weak var lblPing: UILabel!
    @IBOutlet weak var btnFavourite: UIButton!
    
    @IBOutlet weak var btnSelect: UIButton!
    var delegate: ServerCellDelegate!
    var serverInfo: ServerInfo!

    override func awakeFromNib() {
        super.awakeFromNib()
        imgCountry.round()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    func config(server: ServerInfo) {
        self.serverInfo = server
        btnFavourite.tintColor = serverInfo.isFavourite ? UIColor.init(hexString: "#DC3C1C") : UIColor.gray
        lblLocation.text = server.location.uppercased()
        imgCountry.loadUrl(url: server.countryIcon)
        let isSelected = (server.name ==  VPNServerManager.shared.selectServer?.name ?? "")
        self.btnSelect.isHidden = isSelected
        self.contentView.backgroundColor = isSelected ? UIColor.init(hexString: "#1E162A") : UIColor.init(hexString: "#28193F")
        lblPing.text = server.ping == 0 ? "PINGING" : "\(server.ping) MS"
        if (server.ping == 10000) {
            lblPing.text = "No response".uppercased()
        }
    }

    @IBAction func onSelect(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.selectServer(serverInfo: self.serverInfo)
        }
    }
    
    @IBAction func onFavoriteChange(_ sender: Any) {
        self.serverInfo.isFavourite = !self.serverInfo.isFavourite
        btnFavourite.tintColor = serverInfo.isFavourite ? UIColor.init(hexString: "#DC3C1C") : UIColor.gray
        if let delegate = self.delegate {
            delegate.onUpdateFavor(serverInfo: self.serverInfo)
        }
    }
}
