//
//  SettingViewControlller.swift
//  BVPN
//
//  Created by admin on 8/10/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit
let ALWAY_ON_VPN = "ALWAY_ON_VPN"
class SettingViewControlller: BaseViewController {
    
    @IBOutlet weak var switchAutoVPN: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Setting"
        let cancelButton = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(cancel))
        self.navigationItem.leftBarButtonItem = cancelButton
        self.switchAutoVPN.isOn = Util.Storage.getBool(key: ALWAY_ON_VPN)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @objc func cancel() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func actionSave(_ sender: Any) {
        Util.Storage.saveBool(value: switchAutoVPN.isOn, key: "ALWAY_ON_VPN")
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionDeleteAccount(_ sender: Any) {
        self.confirm(title: "Delete account", message: "Your account and all of the data will be DELETED PERMANENTLY!", leftBtn: "Cancel", rightBtn: "Confirm") { indexBtn in
            if indexBtn == 1 {
                self.confirmPasswordAndDeleteAccount()
            }
        }
    }
    
    func confirmPasswordAndDeleteAccount(){
        let ac = UIAlertController(title: "Confirm your password", message: "Please enter your password to continue", preferredStyle: .alert)
        ac.addTextField()
        ac.textFields![0].isSecureTextEntry = true
        ac.textFields![0].placeholder = "Your password"
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { [unowned ac] _ in
          
        }
        ac.addAction(cancelAction)
        let submitAction = UIAlertAction(title: "Submit", style: .default) { [unowned ac] _ in
            let password = ac.textFields![0]
            if password.text == UserRespository.shared.getLastLoginInfo().password {
                self.apiDeleteAccount()
            } else {
                self.alert(title: "Delete account", message: "Wrong password. Please try again")
            }
        }
        ac.addAction(submitAction)
       
        present(ac, animated: true)
    }
    
    func apiDeleteAccount() {
        self.showHUD()
        API.User.deleteAccount { code , json in
            self.hideHUD()
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: Notification.Name.DeleteAccountNotification,object: nil)
            }
            
        } failure: { httpError in
            self.hideHUD()
        }
    }
}
