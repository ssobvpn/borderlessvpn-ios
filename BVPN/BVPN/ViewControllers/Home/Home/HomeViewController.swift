//
//  HomeViewController.swift
//  BVPN
//
//  Created by admin on 7/31/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit
import PlainPing
import NetworkExtension
import Toast_Swift
import ReachabilitySwift
import Fabric
import Crashlytics

class HomeViewController: UIViewController {

    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var imgCountry: UIImageView!

    @IBOutlet weak var imgIPStatus: UIImageView!
    @IBOutlet weak var lblIPAddress: UILabel!
    @IBOutlet weak var btnVPNConnect: XButton!
    @IBOutlet weak var btnChangeServer: XButton!
    var isLastConnected: Bool = false
    let reachability = Reachability()!

    
    //MARK: -  LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        setupVPN()
        getIPAddress()
        updateStatusIP(isHidden: false)
        observerNetwork()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(applicationDidBecomeActive),
                                               name: NSNotification.Name.UIApplicationDidBecomeActive,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(screenLock),
                                               name: NSNotification.Name.UIApplicationWillResignActive,
                                               object: nil)
    }
    
    
    @objc func screenLock() {
//        if !Util.Storage.getBool(key: ALWAY_ON_VPN) {
//            self.disConnectVPN()
//        }
    }
    @objc func applicationDidBecomeActive() {
        if VPNServerManager.shared.currentServer != nil {
            self.connectVPN()
        }
        self.getIPAddress()
    }

    func observerNetwork() {
        reachability.whenReachable = { reachability in
            if reachability.currentReachabilityStatus == .reachableViaWiFi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }

            if VPNServerManager.shared.currentServer != nil {
                self.connectVPN()
            }

            self.getIPAddress()
        }
        reachability.whenUnreachable = { _ in
            print("Not reachable")
            if VPNManager.shared.status == NEVPNStatus.connected {
                self.isLastConnected = true
            }
        }

        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showServerInfo()
    }

    func configUI() {
        lblLocation.numberOfLines = 0
        imgCountry.round()
        imgIPStatus.round()
    }

    func updateServerInfo() {
        self.showServerInfo()
        updateStatusIP(isHidden: VPNManager.shared.status == .connected)
    }

    func updateStatusIP(isHidden: Bool) {
        self.imgIPStatus.backgroundColor = isHidden ? UIColor.green : UIColor.red
        btnChangeServer.setBackgroundImage(isHidden ? Color.tim?.imageRepresentation : Color.primary?.imageRepresentation, for: .normal)
        btnChangeServer.isEnabled = !isHidden

    }

    func updateButtonConnectState(isConnected: Bool) {
        btnVPNConnect.isEnabled = true
        btnVPNConnect.setTitle(isConnected ? "DISCONNECT":"CONNECT", for: .normal)
        btnVPNConnect.setBackgroundImage(isConnected ? Color.tim?.imageRepresentation : Color.primary?.imageRepresentation, for: .normal)
        btnVPNConnect.round(color: UIColor.init(hexString: "#2E164A")!, borderWidth: 4, radius: 8)
    }

    @objc func getIPAddress() {
        DispatchQueue.main.async {
            self.lblIPAddress.text = "CHECKING"
        }
        API.Network.getIpAddress(completion: {[weak self] (_, response) in
            guard let `self` = self else {return}
            self.lblIPAddress.text = response
        }) { [weak self] (error) in
            guard let `self` = self else {return}
            Logger.log(tag: "APIGetIP", content: "Cannot get IP Address :\(error.message())")
            self.lblIPAddress.text = "-"
            if self.reachability.isReachable {
                self.getIPAddress()
            }
        }
    }

    func showServerInfo() {
        DispatchQueue.main.async {
        if let server = VPNServerManager.shared.selectServer {
            self.lblLocation.text = server.location.uppercased()
            self.lblLocation.numberOfLines = 0
            self.imgCountry.loadUrl(url: server.countryIcon)
        }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - VPN functions

    func setupVPN() {
        vpnStateChanged(status: VPNManager.shared.status)
        VPNManager.shared.statusEvent.attach(self, HomeViewController.vpnStateChanged)
    }

    func vpnStateChanged(status: NEVPNStatus) {
        //changeControlEnabled(state: VPNManager.shared.isDisconnected)
        switch status {
        case .disconnected:
            Logger.log(tag: "VPNManager", content: "Disconnected server : \(VPNServerManager.shared.currentServer?.name ?? "")")
            lblMessage.text = ""
            self.isLastConnected = false
            updateButtonConnectState(isConnected: false)
            self.perform(#selector(self.getIPAddress), with: nil, afterDelay: 1)
            updateStatusIP(isHidden: false)
        case .invalid:
            btnVPNConnect.isEnabled = true
            updateStatusIP(isHidden: false)
            getIPAddress()
            lblMessage.text = ""
            Logger.log(tag: "VPNManager", content: "Invalid server : \(VPNServerManager.shared.currentServer?.name ?? "")")
        case .reasserting:
            updateStatusIP(isHidden: false)
            btnVPNConnect.isEnabled = false
            lblMessage.text = "Reasserting..."
        case .connected:
            VPNServerManager.shared.currentServer = VPNServerManager.shared.selectServer
            Logger.log(tag: "VPNManager", content: "Connected to server : \(VPNServerManager.shared.currentServer?.name ?? "")")
            lblMessage.text = "Connected!"
            updateStatusIP(isHidden: true)
            Answers.logCustomEvent(withName: "ConnectVPN", customAttributes: ["Server":VPNServerManager.shared.currentServer?.name,"Email":UserRespository.shared.userInfo?.email ?? ""])
            Util.Storage.saveString(value: VPNServerManager.shared.selectServer?.name ?? "", key: LAST_USED_SERVER)
            self.perform(#selector(self.getIPAddress), with: nil, afterDelay: 1)
            updateButtonConnectState(isConnected: true)
            
            
        case .connecting:
            updateStatusIP(isHidden: false)
            lblMessage.text = "Connecting..."
            btnVPNConnect.isEnabled = false
        case .disconnecting:
            updateStatusIP(isHidden: false)
            lblMessage.text = "Disconnecting..."
        }
    }

    func connectVPN() {
        if (VPNManager.shared.isDisconnected) {
            if let selectedServer = VPNServerManager.shared.selectServer, let user = UserRespository.shared.userInfo {
                let config = Configuration(
                    //server: "dk-ch.borderlessvpn.io",
                    server: selectedServer.pingDNS,
                    account: user.email,
                    password: user.password,
                    onDemand: Util.Storage.getBool(key: ALWAY_ON_VPN),
                    psk: nil)
                Logger.log(tag: "VPNManager", content: "Try to connect to : \(config.server)")
                VPNManager.shared.connectIKEv2(config: config) { error in
                    Logger.log(tag: "VPNManager", content: "Error connect VPN \(error)")
                    let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    alert.show(self, sender: nil)
                }
                config.saveToDefaults()
            } else {
                self.view.makeToast("Please select a vpn server")
            }
        }
    }

    func disConnectVPN() {
        if (!VPNManager.shared.isDisconnected) {
            VPNManager.shared.disconnect()
        }
    }

    // MARK: - UI Actions

    @IBAction func actionChangeServer(_ sender: Any) {
        if let parentVC = self.parent?.parent as? HomeContainerViewController {
            parentVC.gotoPage(index: 1)
        }
    }

    @IBAction func actionConnectVPN(_ sender: Any) {
        if self.btnVPNConnect.currentTitle?.uppercased() == "CONNECT" {
            self.connectVPN()
        } else {
            VPNServerManager.shared.currentServer = nil
            self.disConnectVPN()
        }
    }
    
}
