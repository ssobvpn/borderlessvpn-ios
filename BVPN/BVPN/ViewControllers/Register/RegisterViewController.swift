//
//  RegisterViewController.swift
//  BVPN
//
//  Created by admin on 9/7/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

class RegisterViewController: BaseViewController {

    @IBOutlet weak var txtEmail: XTextField!
    @IBOutlet weak var txtPassword: XTextField!

    @IBOutlet weak var lblTerm: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    // MARK: - Internal functions

    func setupUI() {
        
        self.txtPassword.addDoneOnKeyboardWithTarget(self, action: #selector(self.actionSignUp))
        
        let content = "By tapping \"SIGN UP\" you agree to \"Term & Conditions\" and \"Privacy Policy\" "
        let mutableAttributedString = NSMutableAttributedString.init(string: content, attributes: [NSAttributedString.Key.font: lblTerm.font as Any])
        mutableAttributedString.setAsLink(textToFind: "\"Term & Conditions\"", linkName: "TermLink")
        mutableAttributedString.setAsLink(textToFind: "\"Privacy Policy\"", linkName: "PolicyLink")
        self.lblTerm.attributedText = mutableAttributedString
        self.lblTerm.textColor =  UIColor.white
        self.lblTerm.textAlignment = .center
        self.lblTerm.linkTextAttributes = [NSAttributedString.Key.foregroundColor.rawValue: Color.primary!] as [String : Any]
        self.lblTerm.delegate = self
        self.lblTerm.isEditable = false
        self.lblTerm.isSelectable = true
        self.lblTerm.isUserInteractionEnabled = true
        self.lblTerm.dataDetectorTypes = UIDataDetectorTypes.link
    }
    
   

    func isValid() -> Bool {
        var isValid = true

        if (self.txtEmail.text?.isEmpty_())! {
            isValid = false
            txtEmail.setErrorMessage(errorMessage: "This field is required", animation: false)
        }

        if (self.txtPassword.text?.isEmpty_())! {
            isValid = false
            txtPassword.setErrorMessage(errorMessage: "This field is required", animation: false)
        }

        if !(self.txtPassword.text?.isEmpty_())! && (self.txtPassword.text?.count)! < 5 {
            isValid = false
            txtPassword.setErrorMessage(errorMessage: "This password is too short", animation: false)
        }

        if (!(self.txtEmail.text?.isEmpty_())! && !(self.txtEmail.text?.isValidEmail())!) {
            isValid = false
            txtEmail.setErrorMessage(errorMessage: "Invalid email address", animation: false)
        }

        return isValid
    }

    func hideKeyboard() {
        self.view.endEditing(true)
    }

    // MARK: - API

    @objc func signup() {
        self.view.endEditing(true)
        self.showHUD()
        API.User.signup(email: txtEmail.text!, password: txtPassword.text!, completion: { [weak self] (_, _) in
            guard let `self` = self else {return}
            self.hideHUD()
            Answers.logCustomEvent(withName: "Register_Success", customAttributes: ["email":self.txtEmail.text ?? ""])
            self.view.makeToast("Your account was created successfully. Please login to continue")
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.navigationController?.popViewController(animated: true)
            }
        }) { [weak self] (error) in
            guard let `self` = self else {return}
            self.hideHUD()
            self.txtEmail.setErrorMessage(errorMessage: error.message(), animation: false)
        }
    }

    @objc func callSignUp() {
        self.hideKeyboard()
        if self.isValid() {
            self.signup()
        }
    }

    // MARK: - UI Actions

    @IBAction func actionSignUp(_ sender: Any) {
        self.callSignUp()
    }

    @IBAction func actionLogin(_ sender: Any) {
        self.hideKeyboard()
        self.navigationController?.popViewController(animated: true)
    }

}
extension RegisterViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtEmail {
            self.txtPassword.becomeFirstResponder()
        } else if textField == self.txtPassword {
            self.callSignUp()
        }
        return true
    }
}

@available(iOS 10.0, *)
extension  RegisterViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if URL.absoluteString == "TermLink" {
            print("TermLink")
            self.openTerm()
            return true
        } else if URL.absoluteString == "PolicyLink" {
            print("PolicyLink")
            self.openPolicy()
            return true
        }
        return false
    }
}

