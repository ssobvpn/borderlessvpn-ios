//
//  WebViewViewController.swift
//  BVPN
//
//  Created by admin on 7/31/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit

class WebViewViewController: BaseViewController, UIWebViewDelegate {

    var titleScreen: String!
    var webUrl: String!
    var isLocalFile: Bool? = false
    var isNeedConfirm: Bool = false

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!

    @IBOutlet weak var heightConfirmView: NSLayoutConstraint!
    class func newVC(url: String, title: String,isLocalFile: Bool?,isNeedConfirm:Bool? = false) -> UIViewController {
        let webViewVC = WebViewViewController()
        webViewVC.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        webViewVC.titleScreen = title
        webViewVC.webUrl = url
        webViewVC.isNeedConfirm = isNeedConfirm ?? false
        webViewVC.isLocalFile = isLocalFile
        return webViewVC
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = self.titleScreen
        if let _ = isLocalFile {
            if let url = Bundle.main.url(forResource: webUrl, withExtension: "html") {
                self.webView.loadRequest(URLRequest.init(url: url))
            }
        } else if let url = URL.init(string: self.webUrl) {
            self.webView.loadRequest(URLRequest.init(url: url))
        }
        
        if !isNeedConfirm {
            self.heightConfirmView.constant = 0
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    // MARK: - WebView Delegate

    public func webViewDidStartLoad(_ webView: UIWebView) {
        indicator.startAnimating()
    }

    public func webViewDidFinishLoad(_ webView: UIWebView) {
        indicator.stopAnimating()
    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        indicator.stopAnimating()
    }
    
    @IBAction func actionCancel(_ sender: Any) {
        exit(0)
    }
    
    @IBAction func actionConfirm(_ sender: Any) {
        Util.Storage.saveBool(value: true,key: "AGREE_TERM")
        self.dismiss(animated: true, completion: nil)
    }
}
