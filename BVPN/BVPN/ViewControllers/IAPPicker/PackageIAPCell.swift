//
//  PackageIAPCell.swift
//  BVPN
//
//  Created by admin on 9/22/19.
//  Copyright © 2019 Borderless VPN. All rights reserved.
//

import UIKit
import StoreKit

class PackageIAPCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configUI(product: SKProduct){
        let numberFormatter = NumberFormatter()
        numberFormatter.formatterBehavior = .behavior10_4
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = product.priceLocale
        let priceStr = numberFormatter.string(from: product.price)
        
        lblPrice.text = priceStr
        lblTitle.text = product.localizedTitle
        lblDescription.text = product.localizedDescription
    }
    
}
