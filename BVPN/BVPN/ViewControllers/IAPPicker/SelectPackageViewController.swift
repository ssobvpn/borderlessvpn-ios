//
//  SelectPackageViewController.swift
//  BVPN
//
//  Created by admin on 9/22/19.
//  Copyright © 2019 Borderless VPN. All rights reserved.
//

import UIKit
import StoreKit

protocol SelectPlanDelegate {
    func selectPlan(product:SKProduct)
}

class SelectPackageViewController: UIViewController {
    
    var products:[SKProduct]!
    var delegate:SelectPlanDelegate?
    
    @IBOutlet weak var tableViewPlan: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Select your plan"
        let cancelButton = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(cancel))
        self.navigationItem.leftBarButtonItem = cancelButton
        products.sort{(Double(truncating: $0.price) < Double(truncating: $1.price))}
        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.setupTableView()
    }
    
    func setupTableView() {
        self.tableViewPlan.registerCell(classCell: PackageIAPCell.self)
        self.tableViewPlan.delegate = self
        self.tableViewPlan.dataSource = self
        self.tableViewPlan.reloadData()
        self.tableViewPlan.tableFooterView =  UIView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
//    override func viewDidLayoutSubviews() {
//        if (self.tableViewPlan != nil) {
//            self.tableViewPlan.frame = self.view.frame
//        }
//    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @objc func cancel() {
        self.dismiss(animated: true, completion: nil)
    }
}


// MARK: TableViewDelegate , TableViewDataSource

extension SelectPackageViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: PackageIAPCell = tableView.dequeueReusableCell(withIdentifier: String(describing: PackageIAPCell.self)) as? PackageIAPCell {
            cell.configUI(product: products[indexPath.row])
            cell.layoutMargins = UIEdgeInsets.zero
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let delegate = self.delegate {
            delegate.selectPlan(product: self.products[indexPath.row])
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
