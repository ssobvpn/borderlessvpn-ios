//
//  LoginViewController.swift
//  BVPN
//
//  Created by admin on 7/31/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit
import StoreKit
import Fabric
import Crashlytics

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnForgotPass: UIButton!
    @IBOutlet weak var txtEmail: XTextField!
    @IBOutlet weak var txtPassword: XTextField!
    var selectedProduct: SKProduct?
    
    @IBOutlet weak var lblterm: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    func initInAppPurchase () {
        IAPHelper.shared.fetchAvailableProducts {[weak self] (success, products ) in
            if (success){
                DispatchQueue.main.async {
                    self?.hideHUD()
                    let selectPlanVC = SelectPackageViewController()
                    selectPlanVC.products = products;
                    selectPlanVC.delegate = self
                    let nav = UINavigationController.init(rootViewController: selectPlanVC)
                    nav.modalPresentationStyle = .fullScreen
                    self?.present(nav, animated: true) {}
                }
            } else {
                
            }
        }
        
        IAPHelper.shared.purchaseStatusBlock = {[weak self] (type,transaction) in
            guard let strongSelf = self else { return }
            if type == .purchased {
                strongSelf.showHUD()
                API.Payment.addPaymentHistory(transId: (transaction?.transactionIdentifier)!, completion: { (code, json) in
                    strongSelf.hideHUD()
                    Answers.logPurchase(withPrice: self!.selectedProduct?.price ?? 0.0,
                                        currency: "USD",
                                        success: true,
                                        itemName:self!.selectedProduct?.productIdentifier  ,
                                        itemType: UserRespository.shared.userInfo?.email,
                                        itemId: transaction?.transactionIdentifier,
                                        customAttributes: ["email":UserRespository.shared.userInfo?.email ?? ""])
                    Answers.logCustomEvent(withName: "Purchase_Success", customAttributes: ["email":UserRespository.shared.userInfo?.email ?? "","IAP Type":self!.selectedProduct?.productIdentifier ?? "" ,"Price":self!.selectedProduct?.price ?? 0.0,"Currency":self!.selectedProduct?.priceLocale.currencyCode ?? "USD"])
                    
                    strongSelf.alert(title: "Notice", message: "It can take up to some minutes to active your account.")
                }) { (error) in
                    strongSelf.hideHUD()
                }
                
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // MARK: - Internal functions
    
    func setupUI() {
        //        btnForgotPass.underline()
        if UserRespository.shared.isLastLogged() {
            let lastUser = UserRespository.shared.getLastLoginInfo()
            txtEmail.text = lastUser.email
            txtPassword.text = lastUser.password
            self.login()
        }
        self.txtPassword.addDoneOnKeyboardWithTarget(self, action: #selector(self.actionLogin))
        
        let content = "By tapping \"LOGIN\" you agree to \"Term & Conditions\" and \"Privacy Policy\" "
        let mutableAttributedString = NSMutableAttributedString.init(string: content, attributes: [NSAttributedString.Key.font: lblterm.font as Any])
        mutableAttributedString.setAsLink(textToFind: "\"Term & Conditions\"", linkName: "TermLink")
        mutableAttributedString.setAsLink(textToFind: "\"Privacy Policy\"", linkName: "PolicyLink")
        self.lblterm.attributedText = mutableAttributedString
        self.lblterm.textColor =  UIColor.white
        self.lblterm.linkTextAttributes = [NSAttributedString.Key.foregroundColor.rawValue: Color.primary!] as [String : Any]
        self.lblterm.delegate = self
        self.lblterm.isEditable = false
        self.lblterm.textAlignment = .center
        self.lblterm.isSelectable = true
        self.lblterm.isUserInteractionEnabled = true
        self.lblterm.dataDetectorTypes = UIDataDetectorTypes.link
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserRespository.shared.isLastLogged() {
            let lastUser = UserRespository.shared.getLastLoginInfo()
            txtEmail.text = lastUser.email
            txtPassword.text = lastUser.password
        } else {
            txtEmail.text = ""
            txtPassword.text = ""
        }
    }
    
    func isValid() -> Bool {
        var isValid = true
        
        if (self.txtEmail.text?.isEmpty_())! {
            isValid = false
            txtEmail.setErrorMessage(errorMessage: "This field is required", animation: false)
        }
        
        if (self.txtPassword.text?.isEmpty_())! {
            isValid = false
            txtPassword.setErrorMessage(errorMessage: "This field is required", animation: false)
        }
        
        if !(self.txtPassword.text?.isEmpty_())! && (self.txtPassword.text?.count)! < 5 {
            isValid = false
            txtPassword.setErrorMessage(errorMessage: "This password is too short", animation: false)
        }
        
        if (!(self.txtEmail.text?.isEmpty_())! && !(self.txtEmail.text?.isValidEmail())!) {
            isValid = false
            txtEmail.setErrorMessage(errorMessage: "Invalid email address", animation: false)
        }
        
        return isValid
    }
    
    func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    // MARK: - API
    
    @objc func login() {
        self.view.endEditing(true)
        self.showHUD()
        API.User.login(email: txtEmail.text!, password: txtPassword.text!, completion: { [weak self] (_, json) in
            guard let `self` = self else {return}
            self.getAccountInfo()
        }) { [weak self] (error) in
            guard let `self` = self else {return}
            self.hideHUD()
            self.txtEmail.setErrorMessage(errorMessage: error.message(), animation: false)
        }
    }
    func getAccountInfo() {
        API.User.getAccountInfo(completion: { [weak self] (_, json) in
            guard let `self` = self else {return}
            guard let user = UserInfo.init(json: json) else {return}
            UserRespository.shared.userInfo = user
            UserRespository.shared.userInfo?.password = self.txtPassword.text
            UserRespository.shared.saveLoginInfo()
            
            if user.vpnActive == "1" {
                self.getServerList()
            } else {
                self.hideHUD()
                let alertView = UIAlertController(title: "Notice", message: "Your account was not a subscriber or expired. Please select a plan to continue !", preferredStyle: .alert)
                let action = UIAlertAction(title: "Select plan", style: .default, handler: { (alert) in
                    self.hideHUD()
                    self.initInAppPurchase()
                })
                let actionCancel = UIAlertAction(title: "Cancel", style: .default, handler: { (alert) in
                    
                })
                alertView.addAction(actionCancel)
                alertView.addAction(action)
                self.present(alertView, animated: true, completion: nil)
            }
        }) { [weak self] (_) in
            guard let `self` = self else {return}
            self.hideHUD()
        }
    }
    
    func getServerList() {
        API.VPN.getListServer(completion: { [weak self] (_, json) in
            guard let `self` = self else {return}
            VPNServerManager.shared.getFavourite()
            if let serverList: [ServerInfo] = ServerInfo.listItems(fromJSON: json) as? [ServerInfo] {
                VPNServerManager.shared.serverList = serverList
                VPNServerManager.shared.getSelectServer()
            }
            self.hideHUD()
            self.navigationController?.pushViewController(HomeContainerViewController(), animated: true)
        }) { [weak self] (_) in
            guard let `self` = self else {return}
            self.hideHUD()
        }
    }
    
    @objc func callLogin() {
        self.hideKeyboard()
        if self.isValid() {
            self.login()
        }
    }
    
    // MARK: - UI Actions
    
    @IBAction func actionLogin(_ sender: Any) {
        self.callLogin()
    }
    
    @IBAction func actionForgotPassword(_ sender: Any) {
        self.hideKeyboard()
        //        UIApplication.shared.openURL(URL.init(string: Constant.WebUrl.urlForgotPassword)!)
        self.navigationController?.pushViewController(WebViewViewController.newVC(url: Constant.WebUrl.urlForgotPassword, title: "Forgot password", isLocalFile: false), animated: true)
    }
    
    @IBAction func actionSignUp(_ sender: Any) {
        self.hideKeyboard()
        //        self.navigationController?.pushViewController(WebViewViewController.newVC(url: Constant.WebUrl.urlSignUp, title: "Sign up"), animated: true)
        //UIApplication.shared.openURL(URL.init(string: Constant.WebUrl.urlSignUp)!)
        self.navigationController?.pushViewController(RegisterViewController(), animated: true)
    }
    
}
extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtEmail {
            self.txtPassword.becomeFirstResponder()
        } else if textField == self.txtPassword {
            self.callLogin()
        }
        return true
    }
}

extension LoginViewController: SelectPlanDelegate {
    
    func selectPlan(product: SKProduct) {
        selectedProduct = product
        IAPHelper.shared.purchaseMyProduct(product: product)
    }
    
}

@available(iOS 10.0, *)
extension  LoginViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if URL.absoluteString == "TermLink" {
            print("TermLink")
            self.openTerm()
            return true
        } else if URL.absoluteString == "PolicyLink" {
            print("PolicyLink")
            self.openPolicy()
            return true
        }
        return false
    }
}
