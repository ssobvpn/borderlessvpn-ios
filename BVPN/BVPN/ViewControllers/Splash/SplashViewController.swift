//
//  SplashViewController.swift
//  BVPN
//
//  Created by admin on 7/31/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if (UserRespository.shared.isLastLogged()) {
            UserRespository.shared.userInfo = UserRespository.shared.getLastLoginInfo()
            API.User.login(email: UserRespository.shared.userInfo?.email ?? "", password: UserRespository.shared.userInfo?.password ?? "", completion: { [weak self] (_, _) in
                guard let `self` = self else {return}
                //self.getServerList()
                self.openLoginScreen()
            }) { [weak self] (_) in
                guard let `self` = self else {return}
                self.openLoginScreen()
            }
        } else {
            self.perform(#selector(openLoginScreen), with: nil, afterDelay: 2)
        }
    }

    func getServerList() {
        API.VPN.getListServer(completion: { [weak self] (_, json) in
            guard let `self` = self else {return}
            if let serverList: [ServerInfo] = ServerInfo.listItems(fromJSON: json) as? [ServerInfo] {
                VPNServerManager.shared.serverList = serverList
                VPNServerManager.shared.getSelectServer()
            }
            self.navigationController?.pushViewController(HomeContainerViewController(), animated: true)
        }) { [weak self] (_) in
            guard let `self` = self else {return}
            //UserRespository.shared.logout()
            self.openLoginScreen()
        }

    }

    @objc func openLoginScreen() {
        self.navigationController?.pushViewController(LoginViewController(), animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
