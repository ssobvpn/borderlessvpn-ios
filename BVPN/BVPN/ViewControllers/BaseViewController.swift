//
//  BaseViewController.swift
//  BVPN
//
//  Created by admin on 7/31/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit
import MBProgressHUD

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Hide title back button
        let backItem = UIBarButtonItem()
        backItem.title = ""
        self.navigationItem.backBarButtonItem = backItem
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showHUD() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    
    func hideHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func alert(title: String, message: String) {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let actionOK = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
        })
        alertView.addAction(actionOK)
        self.present(alertView, animated: true, completion: nil)
    }
    
    func confirm(title: String, message: String, leftBtn: String, rightBtn: String, callBack:@escaping ((_ indexBtn: Int) -> Void)) {
        let alert = UIAlertController(title: title.localized, message: message.localized, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: leftBtn.localized, style: .default, handler: {(_) in
            callBack(0)
        }))
        alert.addAction(UIAlertAction(title: rightBtn.localized, style: .default, handler: {(_) in
            callBack(1)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func openTerm() {
        self.navigationController?.pushViewController(WebViewViewController.newVC(url: "term_bvpn" , title: "Term & Conditions", isLocalFile: true), animated: true)
    }
    
    func openPolicy() {
        self.navigationController?.pushViewController(WebViewViewController.newVC(url: "policy_bvpn" , title: "Privacy Policy", isLocalFile: true), animated: true)
    }
    
}
