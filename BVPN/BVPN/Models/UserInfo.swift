//
//  UserInfo.swift
//  BVPN
//
//  Created by admin on 7/31/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit

struct UserInfo: JSONDecoderable {

    var email: String!
    var vpnActive: String!
    var memberShip: String!
    var expiredDate: String!
    var password: String!
    var register: String!
    var token: String!

    init?(json: JSON) {
        email = json["user_email"].stringValue
        vpnActive = json["vpn_active"].stringValue
        token = json["token"].stringValue;
        memberShip = json["user_membership"].stringValue
        register = json["user_registered"].stringValue
        expiredDate =  json["user_membership_expire_date"].stringValue
        
    }
    init?(email: String, password: String,token:String, memberShip: String, register: String) {
        self.email = email
        self.password = password
        self.token = token
        self.memberShip = memberShip
        self.register = register
    }
}
