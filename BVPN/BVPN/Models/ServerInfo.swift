//
//  ServerInfo.swift
//  BVPN
//
//  Created by admin on 7/31/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit

class ServerInfo: JSONDecoderable {

    let name: String
    let location: String
    var pingDNS: String
    var ping: Int = 0
    let countryIcon: String
    var isSelect: Bool = false
    var isFavourite:Bool = false
    
    required init?(json: JSON) {
        name = json["name"].stringValue
        location = json["location"].stringValue
        pingDNS = json["ping_dns"].stringValue
        countryIcon = json["country_icon"].stringValue
        ping = 0
        isFavourite = VPNServerManager.shared.favoriteServers.contains(name)
    }

}
