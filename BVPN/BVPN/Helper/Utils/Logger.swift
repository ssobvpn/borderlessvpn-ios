//
//  Logger.swift
//  BVPN
//
//  Created by admin on 8/3/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit
import Crashlytics

class Logger {
    class func log(tag: String, content: String) {
        NSLog("%@ => %@", tag, content)
        Crashlytics.sharedInstance().setUserEmail(UserRespository.shared.userInfo?.email ?? "")
        Crashlytics.sharedInstance().recordCustomExceptionName(tag, reason: content, frameArray: [])
    }

}
