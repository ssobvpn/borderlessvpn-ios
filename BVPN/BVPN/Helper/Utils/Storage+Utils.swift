//
//  Storage+Utils.swift
//  BVPN
//
//  Created by admin on 8/2/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit

extension Util {
    
    struct Storage {
        
        static func saveString(value: String, key: String) {
            UserDefaults.standard.set(value, forKey: key)
        }
        
        static func getString(key: String) -> String {
            return UserDefaults.standard.string(forKey: key) ?? ""
        }
        
        static func saveBool(value: Bool, key: String) {
            UserDefaults.standard.set(value, forKey: key)
        }
        
        static func getBool(key: String) -> Bool {
            return UserDefaults.standard.bool(forKey: key)
        }
        
        static func saveInt(value: Int, key: String) {
            UserDefaults.standard.set(value, forKey: key)
        }
        
        static func getInt(key: String) -> Int {
            return UserDefaults.standard.integer(forKey: key)
        }
        
        static func removeValue(key: String) {
            UserDefaults.standard.removeObject(forKey: key)
        }
        
        static func clearAllData() {
            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
        }
    }
}
