//
//  UIView+Extensions.swift
//  BVPN
//
//  Created by admin on 8/1/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit

extension UIView {
    func round(color: UIColor, cornerRadius: CGFloat) {
        self.round(color: color, borderWidth: 1.0, radius: cornerRadius)
    }

    func roundCorners(_ corner: UIRectCorner, _ radius: CGFloat) {
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.layer.bounds
        maskLayer.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corner, cornerRadii: CGSize(width: radius, height: radius)).cgPath

        self.layer.mask = maskLayer
        layer.masksToBounds = true
    }

    func round(color: UIColor) {
        self.round(color: color, borderWidth: 1.0, radius: self.frame.height / 2.0)
    }
    func round(color: UIColor, borderWidth: CGFloat) {
        self.round(color: color, borderWidth: borderWidth, radius: self.frame.height / 2.0)
    }
    func round(color: UIColor, borderWidth: CGFloat, radius: CGFloat) {
        self.contentMode = UIView.ContentMode.scaleAspectFill
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = borderWidth
        self.clipsToBounds = true
    }
    func round() {
        self.round(color: .clear)
    }

    func addTapGesture(callBack: Selector) {
        let tap = UITapGestureRecognizer.init(target: self, action: callBack)
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tap)
    }
    func addTapGesture(callBack: Selector, parent: AnyObject?) {
        let tap = UITapGestureRecognizer.init(target: parent, action: callBack)
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tap)
    }
    func addTapGesture(callBack: Selector, parent: AnyObject?, delegate: UIGestureRecognizerDelegate) {
        let tap = UITapGestureRecognizer.init(target: parent, action: callBack)
        tap.delegate = delegate
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tap)
    }
    func addTapGesture(callBack: Selector, viewController: UIViewController) {
        let tap = UITapGestureRecognizer.init(target: viewController, action: callBack)
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tap)
    }
}
