//
//  Data+Extensions.swift
//  Borderless VPN
//
//  Created by Dang Luu on 3/5/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit

extension String {

    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }

    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return substring(from: fromIndex)
    }

    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return substring(to: toIndex)
    }

    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return substring(with: startIndex..<endIndex)
    }

    func isValidEmail() -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSRange(location: 0, length: self.count)) != nil
        } catch {
            return false
        }
    }

    //Min 8 character , contain at least one number and no special character
    func isValidPassword () -> Bool {

        let pat = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$"
        let regex = try! NSRegularExpression(pattern: pat, options: [])
        let matches = regex.matches(in: self, options: [], range: NSRange(location: 0, length: self.count))
        return matches.count > 0
    }

    func isEmpty_() -> Bool {
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).count == 0
    }
}


extension NSMutableAttributedString {
    func setAsLink(textToFind:String, linkName:String) {
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(NSAttributedString.Key.link, value: linkName, range: foundRange)
        }
    }
    @discardableResult func bold(text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont.boldSystemFont(ofSize: 14)]
        let foundRange = self.mutableString.range(of: text)
        if foundRange.location != NSNotFound {
            self.addAttributes(attrs, range: foundRange)
        }
        return self
    }
}

extension Double {
    // Format double
    func toString(decimal: Int) -> String {

        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.roundingMode = .halfUp
//        formatter.groupingSeparator = "."
//        formatter.decimalSeparator = ","
        formatter.minimumFractionDigits = decimal
        formatter.maximumFractionDigits = decimal
        return formatter.string(from: NSNumber.init(value: self))!
    }
    func toCurrency(decimal: Int, currency: String) -> String {
        return self.toString(decimal: decimal) + " " +  currency
    }
}

extension Sequence where Self.Iterator.Element: Numeric {
    var sum: Self.Iterator.Element {
        return self.reduce(0, +)
    }
}

extension Collection where Element: BinaryFloatingPoint {
    var average: Element {
        return self.reduce(0, +) / Element((0 as IndexDistance).distance(to: self.count))
    }
}
