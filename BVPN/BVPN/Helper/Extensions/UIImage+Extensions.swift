//
//  UIImage+Extensions.swift
//  Borderless VPN
//
//  Created by Dang Luu on 4/16/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImage {

    func resizeWithPercent(percentage: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }

    func resizeWithWidth(width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
}

extension UIImageView {
    func loadUrl(url: String) {
        self.loadUrl(url: url, placeHolder: nil)
    }

    func loadUrl(url: String, placeHolder: UIImage?) {
        self.kf.indicatorType = .activity
        let urlImage = URL(string: url)
        self.kf.setImage(with: urlImage, placeholder: placeHolder, options: nil, progressBlock: nil, completionHandler: nil)
    }
}
