//
//  UITableView+Extensions.swift
//  BVPN
//
//  Created by admin on 8/1/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit

extension UITableView {

    func registerCell(classCell: AnyClass) {
        self.register(UINib(nibName: String(describing: classCell), bundle: nil), forCellReuseIdentifier: String(describing: classCell))
    }

    func addRefreshControl(callBack: Selector, viewController: UIViewController) {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(viewController, action: callBack, for: .valueChanged)
        refreshControl.tag = 9898
        self.addSubview(refreshControl)
    }

    func endRefresh() {
        if let refresh = self.viewWithTag(9898) as? UIRefreshControl {
            refresh.endRefreshing()
        }
    }
}
