//
//  UILabel+Hightlight.swift
//  BVPN
//
//  Created by admin on 9/10/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit

extension String {
    
    func hightlighAttibute(text: String, color: UIColor? = Color.blue, font: UIFont? = nil) -> NSMutableAttributedString {
        let titleAttribute = NSMutableAttributedString.init(string: self)
        let hightLightRange = (self as NSString).range(of: text)
        titleAttribute.addAttribute(NSAttributedString.Key.foregroundColor, value: color!, range: hightLightRange)
        if let font = font {
            titleAttribute.addAttribute(NSAttributedString.Key.font, value: font, range: hightLightRange)
        }
        return titleAttribute
    }
    
}

extension UILabel {
    func hightLight(text: String, color: UIColor? = Color.blue) {
        let titleAttr = self.text!.hightlighAttibute(text: text, color: color)
        self.attributedText = titleAttr
    }
}
