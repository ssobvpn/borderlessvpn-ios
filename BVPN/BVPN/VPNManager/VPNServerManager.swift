//
//  VPNServerManager.swift
//  BVPN
//
//  Created by admin on 8/1/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit

let LAST_USED_SERVER = "LAST_USED_SERVER"
let FAVOURITE_SERVERS = "FAVOURITE_SERVERS"

class VPNServerManager {
    var serverList: [ServerInfo] = []
    var selectServer: ServerInfo?
    var currentServer: ServerInfo?
    var favoriteServers:[String] = []
    
    public static let shared = VPNServerManager()
    
    func getSelectServer() {
        let lastUsedServer = Util.Storage.getString(key: LAST_USED_SERVER)
        if lastUsedServer != "" {
            selectServer = serverList.filter({$0.name == lastUsedServer}).first
            currentServer = selectServer
        } else {
            selectServer = serverList[0]
        }
    }
    
    func saveFavourite() {
        Util.Storage.saveString(value: favoriteServers.joined(separator: "#"), key: FAVOURITE_SERVERS)
    }
    
    func getFavourite() {
        favoriteServers =  Util.Storage.getString(key:FAVOURITE_SERVERS).components(separatedBy: "#")
    }
    
}
