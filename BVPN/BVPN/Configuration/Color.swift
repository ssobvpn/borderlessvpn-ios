//
//  Color.swift
//  BVPN
//
//  Created by admin on 7/31/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit

/**
 Colors file.
 */
struct Color {

    // Prevent creating instances outside of struct.
    fileprivate init() {}

    // Application
    static let navigationBarTitleText = UIColor.white
    static let primary = UIColor.init(hexString: "#E55224")
    static let tim = UIColor.init(hexString: "#15082A")

    static let red500   = UIColor(hexString: "F44336")
    static let red100   = UIColor(hexString: "FFCDD2")

    static let gray200  = UIColor(hexString: "9b9b9b")
    static let gray500  = UIColor(hexString: "9E9E9E")
    static let gray100  = UIColor(hexString: "F5F5F5")

    static let chevron  = UIColor(hexString: "FFCDD2")
    static let textRed  = UIColor(hexString: "F44336")

    static let selectedTableViewCell = UIColor(hexString: "D9D9D9")

    // Functional
    static let green    = UIColor(hexString: "259B24")
    static let red      = UIColor(hexString: "df4a5c")
    static let blue     = UIColor(hexString: "53b9cc")
    static let blueLight = UIColor(hexString: "cdfbec")
    static let yellow   = UIColor(hexString: "FFD600")
    static let white    = UIColor.white

    // Name the colors occordingly design guideline name
    static let title        = UIColor(hexString: "FFFFFF")
    static let body1        = UIColor(hexString: "000000")
    static let body2        = UIColor(hexString: "F44336")
    static let body3        = UIColor(hexString: "999999")
    static let subBody      = UIColor(hexString: "999999")
    static let action       = UIColor(hexString: "FFCDD2")
    static let info1        = UIColor(hexString: "999999")
    static let info2        = UIColor(hexString: "999999")
    static let button       = UIColor(hexString: "F44336")
    static let smallLabel   = UIColor(hexString: "F44336")
    static let textField    = UIColor(hexString: "4a4a4a")
    static let textFieldTitleActive    = UIColor(hexString: "527EDB")
    static let menuTitle    = UIColor(hexString: "444444")
    static let spinnerBackground = UIColor(red: 244/255.0, green: 67/255.0, blue: 54/255.0, alpha: 1)

    static let sectionBackground    = UIColor(hexString: "F5F5F5")
    static let sectionTitle         = UIColor(hexString: "F44336")
    static let footerBackground     = UIColor.clear
    static let lineSeparatorTableViewCell        = UIColor(red: 0.783921568627451, green: 0.780392156862745, blue: 0.8, alpha: 1.0)  // Default color in iOS #C8C7CC
    static let appNavBar            = UIColor(hexString: "F44336")

    // Progress bar (under navigator bar)
    static let progressBarPrimaryColor     = UIColor(hexString: "527EBD")
    static let progressBarBackgroundColor  = UIColor(hexString: "F9F9F9")
    struct TableView {
        static let separatorLine    = UIColor.black.withAlphaComponent(0.15)
    }
}
