//
//  AppTheme.swift
//  BVPN
//
//  Created by admin on 7/31/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit

class AppTheme {

    struct Color {
        static let primary = UIColor.init(hexString: "#2b5275")
        static let navColorStart = UIColor.init(hexString: "#2b5275")
        static let navColorEnd = UIColor.init(hexString: "#20354d")
        static let mainBlue = UIColor.init(hexString: "#1A6163")
    }

    struct FontSize {
        static let primary = UIColor.init(hexString: "#1A6163")
        static let mainBlue = UIColor.init(hexString: "#1A6163")
    }

    static let navigationFont =  Font.create(size: 17, medium: true)

}
