//
//  Fonts.swift
//  BVPN
//
//  Created by admin on 7/31/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit

/// Manager that take cares of all font in application.
struct Font {

    static let applicationFontName = "NirmalaUI-Regular"
    static let applicationFontNameMedium = "NirmalaUI-Bold"
    static let applicationFontNameBold = "NirmalaUI-Bold"
    static let applicationFontNameBoldItalic = "NirmalaUI-Regular"

    /**
     Create a font with application font family with given size.
     - parameter size: Size of the font.
     */
    static func create(size: CGFloat, medium: Bool = false) -> UIFont {
        let fontName = medium ? applicationFontNameMedium : applicationFontName
        if let font = UIFont(name: fontName, size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }

    // Dedine common fonts occordingly to design guideline
    static let title        = Font.create(size: 17, medium: true)
    static let body1        = Font.create(size: 17)
    static let body2        = Font.create(size: 15, medium: true)
    static let body3        = Font.create(size: 15)
    static let subBody      = Font.create(size: 12)
    static let action       = Font.create(size: 15, medium: true)
    static let info1        = Font.create(size: 17)
    static let info2        = Font.create(size: 17)
    static let button       = Font.create(size: 17)
    static let smallLabel   = Font.create(size: 10)
    static let textField   = Font.create(size: 18)
    static let titleTextFieldLabel   = Font.create(size: 12)
}

struct FontSize {

    static let title        = CGFloat(17)
    static let body1        = CGFloat(17)
    static let body2        = CGFloat(15)
    static let body3        = CGFloat(15)
    static let subBody      = CGFloat(12)
    static let action       = CGFloat(5)
    static let info1        = CGFloat(17)
    static let info2        = CGFloat(17)
    static let button       = CGFloat(17)
    static let smallLabel   = CGFloat(10)
}
