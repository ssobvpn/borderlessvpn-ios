//
//  Constant.swift
//  BVPN
//
//  Created by admin on 7/31/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit

struct Constant {

    struct WebUrl {
        static let urlSignUp = "https://borderlessvpn.io/"
        static let urlForgotPassword = "https://borderlessvpn.io/my-account/lost-password/"
        static let urlTerm = "https://borderlessvpn.io/terms-and-conditions/"
    }

    struct API {
        static let timeout = 90.0
        static let BASE_URL = "https://api.bvpn.io/"
        static let GET_IP_ADDRESSS = "https://myip.bvpn.io/"
        static let LOGIN = "login"
        static let SIGNUP = "signup"
        static let LOGOUT = "logout"
        static let DELETE_ACCOUNT = "deleteAccount"
        static let GET_ACCOUNT = "account"
        static let GET_SERVER_LIST = "serverlist"
        static let UPDATE_PAYMENT = "payment"

    }

    struct APIAuthen {
        static let user = "bvpn"
        static let password = "mRmpGzCWujJTgwW"
    }

}
