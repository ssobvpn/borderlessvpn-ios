//
//  UserRespository.swift
//  BVPN
//
//  Created by admin on 8/1/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit

class UserRespository {

    public static let shared = UserRespository()

    var userInfo: UserInfo?

    func isLogged() -> Bool {
        if let _ = self.userInfo {
            return true
        }
        return false
    }

    func logout() {
        self.userInfo = nil
         Util.Storage.clearAllData()
    }

    func saveLoginInfo() {
        if let userInfo = self.userInfo {
            Util.Storage.saveString(value: userInfo.email, key: "BVPN_EMAIL")
            Util.Storage.saveString(value: userInfo.password, key: "BVPN_PASSWORD")
            Util.Storage.saveString(value: userInfo.register, key: "BVPN_REGISTER")
            Util.Storage.saveString(value: userInfo.memberShip, key: "BVPN_MEMBERSHIP")
        }
    }

    func isLastLogged() -> Bool {
        let email =   Util.Storage.getString(key: "BVPN_EMAIL")
        return !email.isEmpty
    }

    func getLastLoginInfo() -> UserInfo {
        let email =  Util.Storage.getString(key: "BVPN_EMAIL")
        let password =  Util.Storage.getString(key: "BVPN_PASSWORD")
        let register =  Util.Storage.getString(key: "BVPN_REGISTER")
        let token = Util.Storage.getString(key: "BVPN_TOKEN")
        let memberShip =  Util.Storage.getString(key: "BVPN_MEMBERSHIP")
        return UserInfo.init(email: email, password: password,token: token, memberShip: memberShip, register: register)!
    }
}
