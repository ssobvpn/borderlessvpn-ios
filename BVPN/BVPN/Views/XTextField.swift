//
//  XTextField.swift
//  BVPN
//
//  Created by Dang Luu on 3/15/18.
//  Copyright © 2018 BVPN. All rights reserved.
//

import UIKit

class XTextField: FloatingTextField {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.tintColor = Color.primary!
        self.textColor = Color.white
        self.titleColor = Color.white
        self.lineColor = Color.white
        self.errorColor = Color.red!
        self.errorBackgroundColor = UIColor.clear
        self.selectedTitleColor = Color.white
        self.selectedLineColor = Color.primary!
        self.font = Font.textField
        self.titleLabel.font = Font.titleTextFieldLabel
    }

}
