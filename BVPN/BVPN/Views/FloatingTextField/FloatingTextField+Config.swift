//
//  FloatingTextField+Config.swift
//  BVPN
//
//  Created by Dang Luu on 3/10/18.
//  Copyright © 2018 BVPN. All rights reserved.
//

import UIKit

/// Enum of label styles
enum StyleType {
    case red
    case title
    case body1
    case body2
    case body3
    case subBody
    case action
    case info1
    case info2
    case smallLabel
    case button
    case custom(UIFont, UIColor)

    var font: UIFont {
        switch self {
        case .action:       return Font.action
        case .red:          return Font.title
        case .title:        return Font.title
        case .body1:        return Font.body1
        case .body2:        return Font.body2
        case .body3:        return Font.body3
        case .subBody:      return Font.subBody
        case .info1:        return Font.subBody
        case .info2:        return Font.info2
        case .smallLabel:   return Font.smallLabel
        case .button:       return Font.button
        case .custom(let font, _):
            return font
        }
    }

    var color: UIColor {
        switch self {
        case .action:       return Color.action!
        case .red:          return Color.red500!
        case .title:        return Color.title!
        case .body1:        return Color.body1!
        case .body2:        return Color.body2!
        case .body3:        return Color.body3!
        case .subBody:      return Color.subBody!
        case .info1:        return Color.subBody!
        case .info2:        return Color.info2!
        case .smallLabel:   return Color.smallLabel!
        case .button:       return Color.button!
        case .custom(_, let color):
            return color
        }
    }
}
