//
//  XButton.swift
//  BVPN
//
//  Created by Dang Luu on 3/1/18.
//  Copyright © 2018 BVPN. All rights reserved.
//

import UIKit

@IBDesignable
class XButton: UIButton {

    @IBInspectable var border_width: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = border_width
        }
    }

    @IBInspectable var border_radius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = border_radius
        }
    }

    @IBInspectable var border_color: UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = border_color.cgColor
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        self.titleLabel?.numberOfLines = 0
        self.titleLabel?.lineBreakMode = .byWordWrapping

        UIView.performWithoutAnimation { [unowned self] in

            if let localizableText = self.titleLabel?.text {
                self.setTitle(localizableText.localized, for: .normal)
                self.layoutIfNeeded()
            }
        }
    }

}
