//
//  XLabel.swift
//  BVPN
//
//  Created by Dang Luu on 3/1/18.
//  Copyright © 2018 BVPN. All rights reserved.
//

import UIKit

/**
 Automatically localize text.
 */
@IBDesignable
class XLabel: UILabel {

    @IBInspectable
    var upperCase: Bool = false {
        didSet {
            if let localizableText = self.text {
                self.text = upperCase ? localizableText.localized.uppercased() : localizableText.localized
            }
        }
    }

    @IBInspectable
    var topInset: CGFloat = CGFloat(0)
    @IBInspectable
    var bottomInset: CGFloat = CGFloat(0)
    @IBInspectable
    var leftInset: CGFloat = CGFloat(0)
    @IBInspectable
    var rightInset: CGFloat = CGFloat(0)

    override func awakeFromNib() {
        super.awakeFromNib()

        if let localizableText = self.text {
            if upperCase {
                self.text = localizableText.localized.uppercased()
            } else {
                self.text = localizableText.localized
            }
            //self.font = UIFont.init(name: "Roboto-Regular", size: self.font.pointSize)
        }
    }

    override func drawText(in rect: CGRect) {
        let insets: UIEdgeInsets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        self.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
        
    }

    override public var intrinsicContentSize: CGSize {
        var intrinsicSuperViewContentSize = super.intrinsicContentSize
        intrinsicSuperViewContentSize.height += topInset + bottomInset
        intrinsicSuperViewContentSize.width += leftInset + rightInset
        return intrinsicSuperViewContentSize
    }
}
