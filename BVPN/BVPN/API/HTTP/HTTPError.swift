//
//  HTTPError.swift
//  danglv.hut@gmail.com
//
//  Created by DangLV on 1/10/17.
//  Copyright © 2017 danglv.hut@gmail.com. All rights reserved.
//

enum HTTPError: Error {
    // API ( Http Code , Code , Message)
    case api(Int, String, String)
    case noInternetConnection
    case unknow
    case jsonSerializer

    func title() -> String {
        return ""
    }

    func message() -> String {
        switch self {
        case .noInternetConnection:
            return "Internet connection error"
        case .api(_, _, let message):
            if message.isEmpty {
                return "Internal Error"
            }
            return message
        default:
            return "Internal Error"
        }
    }

    func code() -> String {
        switch self {
        case .noInternetConnection:
            return "internet_connection_error".localized
        case .api(_, let code, _):
            return code
        default:
            return "unknown_error"
        }
    }
}
