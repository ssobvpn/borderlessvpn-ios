//
//  API.swift
//  BVPN
//
//  Created by DangLV on 03/07/18.
//  Copyright © 2018 BVPN. All rights reserved.
//

import UIKit
import Alamofire
import ReachabilitySwift

typealias HTTPCompletion = (Int, JSON) -> Void
typealias HTTPStringCompletion = (Int, String) -> Void
typealias HTTPObjectCompletion<T: JSONDecoderable> = (Int, T) -> Void
typealias HTTPFailure = (_ httpError: HTTPError) -> Void

class API {
    static var shared = API()
    private let reachability = Reachability()!

    var hasConnection: Bool {
        return reachability.isReachable
    }

    fileprivate var _sessionManager: SessionManager

    fileprivate func update(token: String?) {
        _sessionManager = API.createSessionManager(token: token)
    }

    private class func createSessionManager(token: String?) -> SessionManager {
        let certificates = ServerTrustPolicy.certificates()
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            API.baseURL: .pinCertificates(
                certificates: certificates,
                validateCertificateChain: true,
                validateHost: true
            )]

        var headers = Alamofire.SessionManager.defaultHTTPHeaders
        if let token = token {
            headers[APIKey.authorization] = "Bearer \(token)"
        }
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = headers
        configuration.timeoutIntervalForRequest = Constant.API.timeout
        return SessionManager(configuration: configuration, serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies))
    }

    private init() {
        _sessionManager = API.createSessionManager(token: nil)
    }

    static var baseURL: String {
        return Constant.API.BASE_URL
    }

}

// MARK: - Public Infoes

extension API {

    class func update(token: String?) {
        API.shared.update(token: token)
    }

    class func sessionManager() -> SessionManager {
        return API.shared._sessionManager
    }

}
