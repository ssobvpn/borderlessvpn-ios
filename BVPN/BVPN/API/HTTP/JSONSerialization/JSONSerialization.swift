//
//  JSONSerialization.swift
//  BVPN
//
//  Created by Dang Luu on 3/9/18.
//  Copyright © 2018 BVPN. All rights reserved.
//

import UIKit

public protocol JSONDecoderable {

    init?(json: JSON)

    static func listItems(fromJSON json: JSON) -> [Any]
}

extension JSONDecoderable {

    init?(jsonString: String) {
        let json = JSON(parseJSON: jsonString)
        self.init(json: json)
    }

    static func listItems(fromJSON json: JSON) -> [Any] {
        var items = [Self]()

        for jsonItem in json.arrayValue {
            if let item = Self(json: jsonItem) {
                items.append(item)
            }
        }

        return items
    }
}

// MARK: - JSONEncoder

protocol JSONEncoderable {

    func toJSON() -> JSON
}

extension JSONEncoderable {

    func toJSONString() -> String {
        return toJSON().rawString(.utf8, options: []) ?? ""
    }
}
