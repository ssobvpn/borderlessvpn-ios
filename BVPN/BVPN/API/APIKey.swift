//
//  APIKey.swift
//  BVPN
//
//  Created by Dang Luu on 3/9/18.
//  Copyright © 2018 BVPN. All rights reserved.
//

import UIKit

struct APIKey {

    static let data = "data"
    static let errors = "errors"
    static let authorization = "Authorization"
    static let accessToken = "accessToken"
    static let code = "code"
    static let status = "status"
    static let message = "message"
    static let id = "id"
    static let userId = "userId"
    static let createdAt = "createdAt"
    static let email = "email"
    static let user = "user"
    static let userName = "username"
    static let password = "password"
    static let pwd = "pwd"
    static let newPassword = "newPassword"
    static let platform = "platform"
    static let currentPassword = "currentPassword"
    static let transactionId = "transaction_id"
    static let type = "type"
}
