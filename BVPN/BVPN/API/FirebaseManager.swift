//
//  FirebaseManager.swift
//  BVPN
//
//  Created by admin on 12/5/18.
//  Copyright © 2018 Borderless VPN. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

public let USERS: String = "users"
public let CONFIG: String = "config"



final class FirebaseManager: NSObject  {

    public static let shared: FirebaseManager = {
        let instance = FirebaseManager()
        instance.ref = DatabaseReference()
        return instance
    }()
    
    var ref: DatabaseReference!
    
    private override init() {
        super.init()
        
    }
    
    public func savePurchaseInfo(email:String,nextExpired: NSDate,purchaseId:String, complete:(() -> Void)? = nil) {
        //Update next expired
        self.ref.child(USERS).child(email).setValue([:]) { (error, ref) in
            
        }
        //Add purchase history
        self.ref.child(USERS).child(email).setValue([:]) { (error, ref) in
            
        }
    }
    
    public func getUserInfo(email:String, complete:@escaping (() -> Void)) {
        self.ref.child(USERS).child(email).observe(DataEventType.value, with: { (snapshot) in
            let purchaseDict = snapshot.value as? [String : AnyObject] ?? [:]
            complete()
        })
    }
    
}
