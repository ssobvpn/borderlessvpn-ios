//
//  APIErrorCode.swift
//  BVPN
//
//  Created by Dang Luu on 4/18/18.
//  Copyright © 2018 BVPN. All rights reserved.
//

import UIKit

struct APIErrorCode {

    struct User {
        static let incorrectCurrentPassword =  "error_incorrect_current_password"
        static let invalidNewPassword =  "error_invalid_new_password"
    }

}
