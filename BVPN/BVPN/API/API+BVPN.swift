//
//  BVPNAPI.swift
//  BVPN
//
//  Created by DangLV on 07/31/18.
//  Copyright © 2017 API+BVPN.swift. All rights reserved.
//

import UIKit
import Alamofire

extension API {

    struct User {

        static func login(email: String, password: String, completion: @escaping HTTPCompletion, failure: @escaping HTTPFailure) {
            let parameters: [String: Any] =  [APIKey.email: email, APIKey.password: password]
            sendRequest(path: Constant.API.LOGIN, method: .post, params: parameters, completion: completion, failure: failure)
        }

        static func signup(email: String, password: String, completion: @escaping HTTPCompletion, failure: @escaping HTTPFailure) {
            let parameters: [String: Any] =  [APIKey.email: email, APIKey.pwd: password,APIKey.platform: "ios"]
            sendRequest(path: Constant.API.SIGNUP, method: .post, params: parameters, completion: completion, failure: failure)
        }

        static func getAccountInfo(completion: @escaping HTTPCompletion, failure: @escaping HTTPFailure) {
            sendRequest(path: Constant.API.GET_ACCOUNT, completion: completion, failure: failure)
        }

        static func logout(completion: @escaping HTTPCompletion, failure: @escaping HTTPFailure) {
            sendRequest(path: Constant.API.LOGOUT, completion: completion, failure: failure)
        }
        
        static func deleteAccount(completion: @escaping HTTPCompletion, failure: @escaping HTTPFailure) {
            sendRequest(path: Constant.API.DELETE_ACCOUNT, completion: completion, failure: failure)
        }
    }

    struct VPN {

        static func getListServer(completion: @escaping HTTPCompletion, failure: @escaping HTTPFailure) {
            sendRequest(path: Constant.API.GET_SERVER_LIST, method: .get, completion: completion, failure: failure)
        }
    }
    
    struct Payment {
        
        static func addPaymentHistory(transId: String, completion: @escaping HTTPCompletion, failure: @escaping HTTPFailure) {
             let parameters: [String: Any] =  [APIKey.transactionId: transId, APIKey.type: "ios"]
            sendRequest(path: Constant.API.UPDATE_PAYMENT, method: .post, params: parameters, completion: completion, failure: failure)
        }
    }


    struct Network {

        static func getIpAddress(completion: @escaping HTTPStringCompletion, failure: @escaping HTTPFailure) {
            Alamofire.request(Constant.API.GET_IP_ADDRESSS, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).authenticate(user: Constant.APIAuthen.user, password: Constant.APIAuthen.password).responseString { (response) in
                handleStringResponse(response: response, completion: completion, failure: failure)
            }
        }
    }

    // MARK: - Base API Request

    static  func sendRequest(path: String, isCompletedPath: Bool = false, method: HTTPMethod = HTTPMethod.get, params: Parameters? = nil, headers: HTTPHeaders? = Alamofire.SessionManager.defaultHTTPHeaders, encoding: ParameterEncoding? = URLEncoding.default, completion: @escaping HTTPCompletion, failure: @escaping HTTPFailure) {

        if !API.shared.hasConnection {
            failure(HTTPError.noInternetConnection)
            return
        }
        var fullUrl = Constant.API.BASE_URL.appending(path)
        if isCompletedPath {
            fullUrl = path
        }

        var headersForRequest: HTTPHeaders
        if let headersFromRequest = headers {
            headersForRequest = headersFromRequest
        } else {
            headersForRequest = HTTPHeaders()
        }

        NSLog("========REQUEST URL ===== \(fullUrl)")
        NSLog("========REQUEST Header ===== \( headersForRequest)")
        NSLog("========REQUEST Params ===== \( params.debugDescription )")

        Alamofire.request(fullUrl, method: method, parameters: params, encoding: URLEncoding.default, headers: headersForRequest).authenticate(user: Constant.APIAuthen.user, password: Constant.APIAuthen.password).responseJSON { (response) in
            handleResponse(response: response, completion: completion, failure: failure)
        }
    }

    static func handleResponse(response: DataResponse<Any>, completion: @escaping HTTPCompletion, failure: @escaping HTTPFailure) {
        if !API.shared.hasConnection {
            failure(HTTPError.noInternetConnection)
            return
        }
        print("========REQUEST Response ===== \(response.debugDescription)")
        if response.result.isSuccess, let httpCode = response.response?.statusCode, let json  = JSON.init(rawValue: response.result.value as Any) {
            if json[APIKey.status].exists() && json[APIKey.status].stringValue != "success" {
                Logger.log(tag: "APIError", content: json.rawString() ?? "")
                if let errorMessage = json[APIKey.message].string {
                    failure(HTTPError.api(response.response?.statusCode ?? 200, "", errorMessage))
                } else {
                    failure(HTTPError.jsonSerializer)
                }
            } else {
                Logger.log(tag: "APIResponse", content: json.rawString() ?? "")
                completion(httpCode, json)
            }
        } else {
            failure(HTTPError.api(500, "", "Unexpected error"))
        }
    }

    static func handleStringResponse(response: DataResponse<String>, completion: @escaping HTTPStringCompletion, failure: @escaping HTTPFailure) {
        if !API.shared.hasConnection {
            failure(HTTPError.noInternetConnection)
            return
        }
        print("========REQUEST Response ===== \(response.debugDescription)")
        if response.result.isSuccess, let httpCode = response.response?.statusCode, let responseStr = response.result.value {
            completion(httpCode, responseStr)
        } else {
            failure(HTTPError.api(500, "", "Unexpected error"))
        }
    }
}
